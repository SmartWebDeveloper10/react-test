import { render, screen, waitFor,cleanup,fireEvent,findByText } from '@testing-library/react';

import App from '../App';

// test('renders learn react link', () => {
//   render(<App />);
//   const linkElement = screen.getByText(/hacker news./i);
//   expect(linkElement).toBeInTheDocument();
// });



const TOP_STORIES_URL = ' https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty'
const STORY_URL = ' https://hacker-news.firebaseio.com/v0/item'
const AUTHOR_URL = ' https://hacker-news.firebaseio.com/v0/user'


async function mockFetch(url) {
  switch (url) {
    case TOP_STORIES_URL: {
     
      const user = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
      return Promise.resolve({
        ok: true,
        status: 200,
        json: async () => (user),
      })
    }
    //The actual line in this case is `${STORY_URL}/1.json?print=pretty`
    case (url.match(/item/) || {}).input: {
     
        return Promise.resolve({
          ok: true,
          status: 200,
          json: async () => ({
            id: (Math.random() + 1).toString(36).substring(7),
            title: 'Title1',
            url:'ftpgooglecom',
            time: 10000,
            score:123,
            by: 5
          })
        })
      }
      case `${AUTHOR_URL}/5.json?print=pretty`: {
   
        return Promise.resolve({
          ok: true,
          status: 200,
          json: async () => ({
            id:5,
            karma:321
          })
        })
      }  
     
  }
}

jest.mock('../NewsItem.js', () => (props) =>{
  const {
    title,
    url,
    time,
    score,
    authId,
    authKarmaScore,
    imagePath,
    onClickCallback
  } = props



  return (
    <div className="newsItem">      
            <h1>
              Mock News Title:  {title}
            </h1>    
            <button onClick={onClickCallback} >
              ClickMe!
            </button>
            <p>
              Mock URL: {url}
            </p>
            <p>
              Time: {new Date(time).toLocaleDateString("en-US")}
            </p>
            <p>
                Score: {score}
            </p>      
            <img src={`/${imagePath}`}   alt=""/>       
            <h4>
            Mock About the author:
            </h4>
            <p>
                {authId}
            </p>
            <p>
            Mock Karma:  {authKarmaScore}
            </p>

       
    </div>
  )
}  )

beforeAll(() => jest.spyOn(window, 'fetch'))
beforeEach(() => window.fetch.mockImplementation(mockFetch))
afterEach(()=>cleanup())
afterAll(()=> jest.clearAllMocks())

// test('renders learn react link with mock fetch', async () => {
 
//   const {rerender} = render(<App />);

//   await waitFor(() => {
//    // rerender(<App />)
//     const aboutAnchorNode = screen.getAllByText(/ftpgooglecom/i)

//    expect(aboutAnchorNode.length==10).toBeTruthy();
//   //  expect(JSON.stringify(screen)).toHaveTextContent("Karma")
//   },{timeout:300});
  
  
 
 
// });

test('renders learn react link with mock fetch', async () => {
 
  const {rerender} = render(<App />);

  await waitFor(() => {
    expect(screen.getAllByText(/ftpgooglecom/i).length==10).toBeTruthy()
  })

  
  fireEvent.click(screen.getAllByRole('button')[0])
  fireEvent.click(screen.getAllByRole('button')[1])

  await waitFor(() => {
    expect(screen.getByText('Likes: 2')).toBeInTheDocument()
  })
  
 
 
 
});

test('renders learn react link with mock fetch and click', async () => {
 
  const {rerender} = render(<App />);

  await waitFor(() => {
   // rerender(<App />)
    const titles = screen.getAllByText(/Mock News Title/i)
    const urls = screen.getAllByText(/ftpgooglecom/i)

   expect(titles.length==10).toBeTruthy();
   expect(urls.length==10).toBeTruthy();
  //  expect(JSON.stringify(screen)).toHaveTextContent("Karma")
  },{timeout:300});
  
  
 
 
});


