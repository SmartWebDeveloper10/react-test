
import { render, screen, waitFor } from '@testing-library/react';

import App from '../App';
test('renders learn react link with live fetch', async () => {
 
    const {rerender} = render(<App />);
  
    await waitFor(() => {
     // rerender(<App />)
      const urls = screen.getAllByText(/https/i)
      const authors = screen.getAllByText(/author/i)
  
     expect(urls.length>0).toBeTruthy();
     expect(authors.length==10).toBeTruthy();
    //  expect(JSON.stringify(screen)).toHaveTextContent("Karma")
    },{timeout:5000});
    
    
   
   
  });