import React, { useEffect, useState } from 'react';
import './App.scss';

import NewsItem from './NewsItem';


const TOP_STORIES_URL = ' https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty'
const STORY_URL = ' https://hacker-news.firebaseio.com/v0/item'
const AUTHOR_URL = ' https://hacker-news.firebaseio.com/v0/user'


function App() {
 
  const [hackerNewsList, setHackerNewsList] = useState([])
  const [counter,setCounter] = useState(0);

  const NUM_STORIES = 10;




  useEffect(()=>{
    fetch(TOP_STORIES_URL)
    .then(res => res.json())
    .then(topNewsArr=>{
      let slicedTopStories = topNewsArr.slice(0, NUM_STORIES);
      Promise.all(slicedTopStories.map(storyId => fetch(
        `${STORY_URL}/${storyId}.json?print=pretty`
       
      )))
      .then(resp => Promise.all( resp.map(r => r.json()) ))
      .then(storiesArr =>{
        
        Promise.all(storiesArr.map(sId => fetch(
          `${AUTHOR_URL}/${sId.by}.json?print=pretty`
         
        )))
        .then(resp => Promise.all( resp.map(r => r.json()) ))
        .then(authorList =>{
          let finalStories = [];

          finalStories = storiesArr.map(st=>{
            let au = authorList.find(a=>st.by === a.id)
            st["authId"] = au.id;
            st["authKarmaScore"] = au.karma;
            st["imagePath"] = "hacker.jpg";
            st["onClickCallback"] = handleClick;
            return st; 

          }).sort((a,b)=> b.score - a.score);

          setHackerNewsList(finalStories)
          
        });        
      })
    
    })
  }
, [])



function handleClick(e){
 
  setCounter(counter=>counter+1)
}

  return (
    <div className="appBody">
      <h1>Hacker News</h1>
  {hackerNewsList.length>0 ? <h3 className="likes">Likes: {counter}</h3>:<p></p>}
      
      {hackerNewsList.length>0 ? hackerNewsList.map((hn)=>{
         return( <NewsItem key={hn.id} {...hn} />)
      }):<h2>Loading Top Hacker News. Please wait....</h2>}

    
    </div>
  );
}

export default App;
